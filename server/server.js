'use strict';

const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors');
const expressjwt = require('express-jwt');
const blacklist = require('express-jwt-blacklist');

const config = require('./config');

const Logger = require('node-json-logger');
const logger = new Logger({ level: config.logerLevel});

// Si se cambia de version se modifica la ruta.
const api = require(`./api/${config.API_VERSION}api`);

const app = express();

app.use(cors()); // Enable corse
app.use(morgan('common')); // Setup middleware
app.use(bodyParser.urlencoded({ extended: false })); // parse application/x-www-form-urlencoded 
app.use(bodyParser.json()); // parse application/json 

const pathAuth = `${config.API_BASE_URL}${config.API_VERSION}`;
console.log("pathAuth:" + pathAuth);

app.use(expressjwt({
  secret: config.JWT_SECRET,
  isRevoked: blacklist.isRevoked
  }
).unless({path: [pathAuth] }));

// Setup router and routes.
app.use(config.API_BASE_URL, api); // Example:  http://localhost:3000/api/users
app.use(`${config.API_BASE_URL}${config.API_VERSION}`, api); // Example: http://localhost:3000/api/v1/users

logger.info(`URL Base de la Api en local: localhost:${config.API_PORT}${config.API_BASE_URL}`);
logger.info(`URL por version en local: http://localhost:${config.API_PORT}${config.API_BASE_URL}${config.API_VERSION}`);

app.use( (req, res, next) => {
  logger.info('Ruta no encontrada');
  res.status(404);
  res.json({'error': 'Error. Ruta no encontrada'});
});

app.use( (err, req, res, next) => {
  logger.error('Error');
  res.status(500);
  res.json({'error': `${err}`});
});

module.exports = app;