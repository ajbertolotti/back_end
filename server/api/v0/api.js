
var express = require('express'); // Importe el modulo
var bodyParser = require('body-parser');
var app = express(); // Creo objeto de la clase express
app.use (bodyParser.json());
var port =  process.env.PORT || 3000; // Puerto definido en variable de entorno o en su defecto

const URI = '/api-uruguay/v1';

var usersFile = require('../../..//mocks/users.json');
var accountsFile = require('../../../mocks/accounts.json');

app.get(URI + '/',
  function(req, res) {
    res.send("Hola api desde node..");
});

/********************************************************************
****************************** USERS ********************************
*********************************************************************/

// GET users
app.get(URI + '/usersOld',
  function(req, res) {
    res.send(usersFile);
  });

  // GET users
app.get(URI + '/users',
function(req, res) {

  // http://localhost:3000/api-uruguay/v1/users?first_name=Conn&last_name=Meyer&email=cmeyer3@xinhuanet.com
  // http://localhost:3000/api-uruguay/v1/users
  
  // Retorna un usuario si tiene los atributos enviados como query params
  function filterUsers(us, queryParams) {
    for (var param in queryParams) {
      if (queryParams[param] != us[param]) {
        return false;
      }
    }
    return true;
  }
  let _usuarios = usersFile.filter( us =>
    filterUsers(us, req.query)
  );

  console.log( filterUsers(usersFile[3], req.query) );
  console.log ("Cant Usuarios filtrados:" + _usuarios.length);
  _usuarios.forEach(element => {
    console.log (`Id Usuario: " ${element.id} - Nombre: ${element.first_name} - Apellido: ${element.last_name}  `);
  });

  res.send(_usuarios);
});

// GET a user con query string. Funciona ok!
app.get(URI + '/XXXuser?',
  function(req, res) {
    console.log("GET con query string")
    console.log(req.query);
    console.log(req.query.first_name);
    console.log(req.query.last_name);
    let msgResponse = {'msg' : 'Usuario no encontrado'};

    function filterUsers(query) {
      return usersFile.filter(function(us) {
        return (us.first_name === query.first_name && us.last_name === query.last_name);
      })
    }

    console.log("con filterItems....");
    let usuariosEncontrados = filterUsers(req.query);
    console.log("Usuarios filtrados:" + usuariosEncontrados);
    //console.log(filterItems(req.query));
    console.log("fin filterItems....");
    if (usuariosEncontrados.length > 0) {
      msgResponse = {'msg' : 'Usuarios encontrados', usuariosEncontrados};
    }

    res.send(msgResponse);
    //res.status(statusCode).send(msgResponse);
});

// GET a user
app.get(URI + '/users/:id',
  function(req, res) {
    let idUser = req.params.id;
    let us = usersFile[idUser - 1];
    console.log('Usuario:' + us);
    res.send(us);
  });

// POST users. Id se crea a partir del inidice del array, no se envía.
app.post(URI + '/users',
  function(req, res) {
    console.log('POST users..');
    console.log(req.body);
    let _nuevoUsuario = req.body;
    _nuevoUsuario.id = usersFile.length+1;
    console.log(_nuevoUsuario);
    usersFile.push(_nuevoUsuario); // Lo agrego al array
    let msg = {'message': 'Nuevo usuario agregado correctamente'};
    res.send({ msg, _nuevoUsuario});
});

// DELETE, borra el usuario indicado.
app.delete(URI + '/users/:id', function(req, res) {
  usuarios.splice(req.params.id-1, 1)
  res.send("Usuario borrado")
});

// PUT a user, modificar datos
app.put(URI + '/users/:id',
  function(req, res) {
    console.log('PUT users..');
    let idUserUpdate = Number(req.params.id);
    let usDb = usersFile[idUserUpdate-1];
    let statusCode = 200;
    let msgResponse = {'msg' : 'Update ok'};
    if (usDb && (idUserUpdate === usDb.id) ) { // Existe el usuario
      console.log('El usuario existe, actualizo sus datos. Id Usuario: ' + usDb.id);
      let _nuevosDatosUsuario = req.body;
      usDb.first_name = _nuevosDatosUsuario.first_name;
      usDb.last_name = _nuevosDatosUsuario.last_name;
      usDb.email = _nuevosDatosUsuario.email;
      usDb.password = _nuevosDatosUsuario.password;
    } else {
      console.log('El usuario NO existe..');
      statusCode = 400;
      msgResponse = {'msg' : 'Erorr en update, el usuario no existe'};
    }
    res.status = statusCode;
    res.send(msgResponse);
});


/********************************************************************
****************************** ACCOUNTS *****************************
*********************************************************************/

// GET accounts
app.get(URI + '/accounts',
  function(req, res) {
    res.send(accountsFile);
});

// GET an account
app.get(URI + '/accounts/:id',
  function(req, res) {
    let id = req.params.id;
    let account = accountsFile[id-1];
    res.send(account);
});


/********************************************************************/

app.listen(port);
console.log(`App escuchando puerto ${port}`);
