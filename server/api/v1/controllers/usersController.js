'use strict';
const config = require('../../../config'); 

var requestJSON = require('request-json');
const Logger = require('node-json-logger');
const logger = new Logger({ level: config.logerLevel});


const API_BASE_URL = config.API_BASE_URL;
const baseMLabURL = config.baseMLabURL;
const apiKey = config.apiKey;
const apikeyMLab = `apiKey=${apiKey}`;

// get All Users
exports.getAll = (req, res, next) => {
    logger.info(`GET ${API_BASE_URL}users`);
    var httpClient = requestJSON.createClient(baseMLabURL);
    let expand = req.query.expand;
    let  queryString = 'f={"_id":0,"password":0, "accounts":0, "accounts.transactions":0}&';
    if (expand == "accounts") {  // Example: http://localhost:3000/api-uruguay/v1/users?expand=accounts
        queryString = 'f={"_id":0,"password":0, "accounts.transactions":0}&';
    } else if ( expand == "all") { // Example: http://localhost:3000/api-uruguay/v1/users?expand=all
        queryString = 'f={"_id":0,"password":0}&';
    }

    //logger.info(`Cliente HTTP mLab creado.`);
    httpClient.get('user?' + queryString + apikeyMLab,
      function(err, respuestaMLab, body) {
        var response = {};
        if(err) {
            res.status(500).send('Error obteniendo usuario');
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            res.status(404).send('Usuario no encontrado');
          }
        }
        res.send(response);
        //res.json(response);
      });
};

// get a user
exports.get = (req, res, next) => {
    let id = req.params.id;
    let expand = req.query.expand;
    console.log(`expand: ${expand}`);
    let queryString = 'q={"userId":' + id + '}&f={"_id":0,"password":0, "accounts":0, "accounts.transactions":0}&';
    if (expand == "accounts") {
        console.log("accounts");
        // Example: http://localhost:3000/api-uruguay/v1/users/2?expand=accounts
        queryString = 'q={"userId":' + id + '}&f={"_id":0,"password":0, "accounts.transactions":0}&';
    } else if ( expand == "all") {
        // Example: http://localhost:3000/api-uruguay/v1/users/2?expand=all
        queryString = 'q={"userId":' + id + '}&f={"_id":0,"password":0}&';
    }
    logger.info(`Id usuario requerido ${id}`);
    logger.info(`GET ${ API_BASE_URL }users/${id}`);
    var httpClient = requestJSON.createClient(baseMLabURL);
    //console.log("Cliente HTTP mLab creado.");

    httpClient.get('user?' + queryString + apikeyMLab,
        function(err, respuestaMLab, body) {
            var response = {};
            if (err) {
                res.status(500).send('Error obteniendo usuario');
            } else {
                if (body.length > 0) {
                    response = body[0];
                } else {
                    res.status(404).send('Usuario no encontrado');
                }
            }
            res.send(response);
        });
};

// add new user
exports.post = (req, res, next) => {
    let hashedPassword = req.headers.authorization;
    if (hashedPassword == undefined) {
        res.status(400).send('Bad request, falta header authorization');
    } else {
        let jwtUser = req.user; // Tengo aqui la información del usuario logado incluida en el jwt
        //console.log(jwtUser);
        if (!jwtUser || jwtUser.rol != 'admin') {
            res.status(401);
            res.json({'error': `Usuario no es administrador por lo que no está habilitado a realizar alta de usuarios`});
        } else {
            let newUser = req.body;
            newUser.isLogged = false;
            logger.info(`POST ${ API_BASE_URL }users`);
            let httpClient = requestJSON.createClient(baseMLabURL);

            // Verifico si ni existe otro usuario con el mismo mail.
            let queryString = 'q={"email": "' + req.body.email + '"}&f={"_id":0, "userId":1, "first_name":1}&';
            httpClient.get('user?' + queryString + apikeyMLab,
                function(err, respuestaMLab, body) {
                    console.log(queryString);
                    console.log(body);
                    if (err) {
                        res.status(500).send('Error obteniendo buscando usuario con mismo mail.');
                    } else {
                        if (body.length > 0) {
                            console.log(`error, ya existe usuario con mail ingresado, userId: ${body[0].userId}`);
                            res.status(422);
                            res.json({'error': `Ya existe usuario con mail ingresado, userId: ${body[0].userId}`});
                        } else {
                            // Obtengo el mayor userId
                            queryString = 'f={"_id":0, "userId":1}&l=1&s={"userId":-1}&';
                            httpClient.get('user?' + queryString + apikeyMLab,
                            function(err, respuestaMLab, body) {
                                var response = {};
                                let maxUsId = 0;
                                if (err) {
                                    res.status(500).send('Error obteniendo usuario desde Mlab');
                                } else {
                                    if (body.length > 0) {
                                        maxUsId = Number(body[0].userId);
                                    } else {
                                        logger.info(`No se encontraron usuarios, se asume se esta insertando el primero`);
                                    }
                                }
                                logger.info(`Id Mayor usuario encontrado: ${ maxUsId }`);
                                newUser.userId = maxUsId + 1;
                                newUser.accounts = [];
                                httpClient.post(baseMLabURL + 'user?' + apikeyMLab, newUser,
                                    function(err, respuestaMLab, body) {
                                        if (err) {
                                            res.status(500);
                                            res.json({'error': `Mlab - Error al intentar realizar alta de usuario`});
                                        } else {
                                            //console.log("Pasw:" + newUser.password);
                                            response.userId = body.userId;
                                            res.send(response);
                                        }
                                    });
                            });
                        }
                    }
            });
        }
    };
};

// update a user
exports.put = (req, res, next) => {
    let userId = req.params.id;
    let newUser = req.body;
    newUser.userId = Number(userId);
    let queryString = 'q={"userId":' + userId + '}&f={"_id":1,"userId":1, "password":1, "email":1, "accounts":1}&';
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('user?' + queryString + apikeyMLab,
        function(err, respuestaMLab, body) {
            if (err) {
                res.status(500).send('Mlab - Error obteniendo usuario');
            } else {
                if (body.length > 0) {
                    console.log('Actualizacion de usuario!');
                    console.log('email: ' + body[0].email);
                    newUser.password = body[0].password; // No se permite cambiar
                    newUser.email = body[0].email; //  No se permite cambiar
                    newUser.accounts = body[0].accounts; //  No se permite cambiar
                    let idUpdate = body[0]._id.$oid;
                    let urlPut = 'user/' + idUpdate + '?' + apikeyMLab;
                    httpClient.put(urlPut, newUser, (err, resMLab, bodyP) => {
                        if (err) {
                            res.status(500).send('Mlab - Error al intentar actualizar usuario');
                        } else {
                            newUser.password = undefined; // Para que no viaje en la respuesta
                            res.status(200).send({ "message": "Usuario actualizado", newUser });
                        }
                    });
                } else {
                    res.status(404).send('Usuario no encontrado');
                }
            }
        });
};

exports.delete = (req, res, next) => {
    let userId = req.params.id;
    let queryString = 'q={"userId":' + userId + '}&';
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('user?' + queryString + apikeyMLab,
        function(err, respuestaMLab, body) {
            if (err) {
                res.status(500).send({"message" : "Error al obtener el usuario de Mlab"});
            } else {
                console.log(body);
                if (body.length > 0) {
                    let idDelete = body[0]._id.$oid;
                    let urlDelete = "user/" + idDelete + "?" + apikeyMLab;
                    httpClient.delete(urlDelete, (err, respuestaMLab, body) => {
                        if (!err)
                            res.send({"message": "Usuario eliminado"});
                        else
                            res.status(500).send({"message": "Error", err});
                        });
                } else {
                    res.status(404).send({"message" : "No existe el usuario"});
                }
            }
        });
};