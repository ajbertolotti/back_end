'use strict';
const config = require('../../../config');

var requestJSON = require('request-json');
const Logger = require('node-json-logger');
const logger = new Logger({ level: config.logerLevel});

const API_BASE_URL = config.API_BASE_URL;
const baseMLabURL = config.baseMLabURL;
const apiKey = config.apiKey;
const apikeyMLab = `apiKey=${apiKey}`;
const EXCHANGE_RATE = config.EXCHANGE_RATE;
// 
exports.postTransfer = (req, res, next) => {
    let hashedPassword = req.headers.authorization;
    let transfer = req.body;
    let senderUser;
    let reciverUser;
    let senderUserAccountId = 0;
    let reciverUserAccountId = 0;
    let response = '';

    if (hashedPassword == undefined) {
        response = 'Bad request, falta header authorization'; // Solo dejo este mensaje a modo de demo..
        res.status(400).send(response);
        logger.info(response);      
    } else {   
        logger.info(`POST ${API_BASE_URL}transfer`);
        let jwtUser = req.user; // Tengo aqui la información del usuario logado incluida en el jwt
        let httpClient = requestJSON.createClient(baseMLabURL);

        //let queryString = 'q={"userId":' + transfer.sender.userId + '}&f={"_id":0}&';
        let queryString = 'q={"userId":' + transfer.sender.userId + '}&';

        httpClient.get('user?' + queryString + apikeyMLab,
          function(err, respuestaMLab, body) {
            if(err) {
                response = 'Error obteniendo cliente origen de transferencia';
                res.status(500).send(response);
                logger.info(response);
            } else {
                if(body.length > 0) { // Existe el usuario origen de transferencias
                    senderUser = body[0];
                    if (jwtUser.userId == senderUser.userId ) { // Usuario logado es el mismo de orgien de transferncia
                        if (senderUser.accounts && senderUser.accounts.length > 0) {
                            senderUserAccountId = getAccount(senderUser, transfer.sender.accountId, res);
        
                            // Voy por el reciver
                            let queryString = 'q={"userId":' + transfer.reciver.userId + '}&';
                            httpClient.get('user?' + queryString + apikeyMLab,
                            function(err, respuestaMLab, body) {
                                if(err) {
                                    response = 'Error obteniendo cliente destino de transferencia';
                                    res.status(500).send(response);
                                    logger.info(response);
                                } else {
                                    if(body.length > 0) { // Existe el usuario destino de transferencias
                                        reciverUser = body[0];
                                        if (reciverUser.accounts && reciverUser.accounts.length > 0) {
                                            reciverUserAccountId = getAccount(reciverUser, transfer.reciver.accountId, res);
                                            response = 'Están dadas las condiciones básicas para realizar la transferencia..';
                                            logger.info(response);
    
                                            relizarTransferencia(transfer, senderUser, senderUserAccountId, reciverUser, reciverUserAccountId, res);
    
                                        } else {
                                            response = 'Reciver Error, Cliente destino de transferencia no tiene cuentas';
                                            res.status(500).send(response);
                                            logger.info(response);      
                                        }
                                    } else {
                                        response = 'Reciver Error - Cliente destino incorrecto';
                                        res.status(422).send(response);
                                        logger.info(response);
                                    }
                                }
                            });
                        }
                        else {
                            response = 'Sender Error, Cliente ordenante de transferencia no tiene cuentas';
                            res.status(500).send(response);
                            logger.info(response);                         
                        }
                    } else {
                        response = 'Usuario origen de transferencia debe ser el usuario logado';
                        res.status(403).send(response);
                        logger.info(response);      
                    }
                } else {
                    response = 'Sender Error - Cliente origen incorrecto';
                    res.status(422).send(response);
                    logger.info(response);
                }
            }
        });
    };

};;


    // Dada una cuenta retorna el mayor id de transaccion que tenga.
    function maxTransferId (account) {
        let maxId = 0;
        if (account && account.transactions) {
            account.transactions.forEach(function(transaction) {
                if (transaction.transactionId > maxId) {
                    maxId = transaction.transactionId;
                }
            });
        }
        return maxId;
    };

    function getAccount(userData, accountId, res) {
        let i = 0;
        let encontre = false;
        if (userData && userData.accounts) {
            while (i < userData.accounts.length && !encontre ) {
                if (userData.accounts[i].accountId == accountId) {
                    res.account = userData.accounts[i];
                    encontre = true;
                };
                i++;
            }
        }
        // Si el usuario existe retorno su posición el array, en casi contrario retorna 0
        return encontre ? i-1 : 0; 
    };

    function getExchangeRate(originAmount, originCurrency, destinationCurrency) {
        if (originCurrency == destinationCurrency) {
            return originAmount;
        } else if (originCurrency = 'UYU') {
            return (originAmount / EXCHANGE_RATE);
        } else {
            return (originAmount * EXCHANGE_RATE);
        }
    };

    // Efectiviza la transferencia, realiza los put
    function relizarTransferencia(transfer, senderUser, senderUserAccountId, reciverUser, reciverUserAccountId, res) {
        let response = '';
        let senderTransaction = {
            transactionId: maxTransferId(senderUser.accounts[senderUserAccountId]) + 1,
            amount: getExchangeRate(transfer.sentMoney.amount, transfer.sentMoney.currency, senderUser.accounts[senderUserAccountId].currency), 
            currency: senderUser.accounts[senderUserAccountId].currency, 
            concept: transfer.concept, 
            type: 'own_transfer',
            credit_debit: 'D',
            date: new Date()
        };
        // Verifico saldo suficiente en ordenante
        if (senderUser.accounts[senderUserAccountId].available_balance < senderTransaction.amount) {
            response = 'Sender Error, saldo cliente ordenante insuficiente';
            res.status(422).send(response);
            logger.info(response);        
        } else 
        {
            if (!senderUser.accounts[senderUserAccountId].transactions) { // Si la cuenta no tiene transacciones creo el array
                senderUser.accounts[senderUserAccountId].transactions = [];
            } 
            senderUser.accounts[senderUserAccountId].transactions.push(senderTransaction); // Agrego la transaccion al array
            
            if (transfer.sender.userId == transfer.reciver.userId) {
                reciverUser = senderUser;
            }
            let receiverTransaction = {
                transactionId:  maxTransferId(reciverUser.accounts[reciverUserAccountId]) + 1,
                amount: getExchangeRate(transfer.sentMoney.amount, transfer.sentMoney.currency, reciverUser.accounts[reciverUserAccountId].currency), 
                currency: reciverUser.accounts[reciverUserAccountId].currency, 
                concept: transfer.concept, 
                type: 'own_transfer',
                credit_debit: 'C',
                date: new Date()
            };
            if (!reciverUser.accounts[reciverUserAccountId].transactions) { // Si la cuenta no tiene transacciones creo el array
                reciverUser.accounts[reciverUserAccountId].transactions = [];
            }
            reciverUser.accounts[reciverUserAccountId].transactions.push(receiverTransaction); // Agrego la transaccion al array
    
            // Incremento saldo de beneficiario y lo decremento en ordenante.
            senderUser.accounts[senderUserAccountId].available_balance -= senderTransaction.amount;
            reciverUser.accounts[reciverUserAccountId].available_balance += receiverTransaction.amount;

            var httpClient = requestJSON.createClient(baseMLabURL);       
            let idUpdate = senderUser._id.$oid;
            let urlPut = 'user/' + idUpdate + '?' + apikeyMLab;
            console.log(JSON.stringify(senderUser.accounts[0]));
            httpClient.put(urlPut, senderUser, (err, resMLab, bodyP) => {
                if (err) {
                    response = 'Error al ingresar transferencia - origen';
                    res.status(500).send(response);
                    logger.info(response);
                } else {
                    logger.info('bodyP:' + bodyP);
                    idUpdate = reciverUser._id.$oid;
                    urlPut = 'user/' + idUpdate + '?' + apikeyMLab;
                    httpClient.put(urlPut, reciverUser, (err, resMLab, bodyP) => {
                        if (err) {
                            response = 'Error al ingresar transferencia - destino';
                            res.status(500).send(response);
                            logger.info(response);
                        } else {
                            res.status(200).send('TRANSFERENCIA REALIZADA CON EXITO!!');
                        }
                    });
                }
            });
        }
    };


// .........................................................


/*
 * /api/transfer/        POST   - CREATE a transfer. 
 * 
 * Sender user, sender account, reciver user and reciver account are part of the body
 * Body example:
{ 
 "concept": "Paying for my beers",
 "sentMoney": { "amount": 343, "currency": "UYU" },
 "sender": { 
     "userId" : 1234,
     "accountId": 3243
 },
 "reciver": { 
    "userId" : 1234,
    "accountId": 3243
 }
}
 */