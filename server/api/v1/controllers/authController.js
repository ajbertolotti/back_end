'use strict';
const config = require('../../../config'); 

const requestJSON = require('request-json');
const Logger = require('node-json-logger');
const logger = new Logger({ level: config.logerLevel});
const jwt = require('jsonwebtoken');

const blacklist = require('express-jwt-blacklist');

const API_BASE_URL = config.API_BASE_URL;
const baseMLabURL = config.baseMLabURL;
const apiKey = config.apiKey;
const apikeyMLab = `apiKey=${apiKey}`;

function genereateJWT(userData) {
    let jwtSingData = {
        sub: `${userData.userId}${userData.email}`,
        userId: userData.userId,
        email: userData.email,
        rol: userData.rol
    };
    var token = jwt.sign(jwtSingData, config.JWT_SECRET);
    //console.log(token);
    return token;
};

function generateLoginResponse(userData, res) {
    let token = genereateJWT(userData);
    res.set('Autorization', token); // Asingo el jwt al header autorization.
    res.status(200).send(
        {
            "userId": userData.userId, 
            "userName": userData.first_name, 
            "jwt": token,
            "email": userData.email,
            "avatar": userData.avatar,
            "rol": userData.rol
        });
    return res;
};


// LOGIN
exports.login = (req, res, next) => {
    let hashedPassword = req.headers.authorization;
    //let userId = req.headers.user;
    let email = req.headers.email;

    if (hashedPassword == undefined) {
        res.status(400).send('Bad request, falta header authorization');
    } else {
        logger.info(`POST ${API_BASE_URL}auth`);
        let httpClient = requestJSON.createClient(baseMLabURL);
        let queryString = 'q={"email":"' + email + '"}&f={"_id":1,"userId":1, "password":1, "rol":1, "first_name":1,"email":1, "avatar":1 }&';

        httpClient.get('user?' + queryString + apikeyMLab,
          function(err, respuestaMLab, body) {
            if(err) {
                res.status(500).send('Mlab - Error obteniendo usuario');
            } else {
                if(body.length > 0) {
                    //console.log(body[0]);
                    //console.log("passw de base: " + body[0].password);
                    //console.log("passw recibida: " + hashedPassword);                    
                    body[0].password === hashedPassword ? generateLoginResponse(body[0], res) : res.status(401).send('No autorizado');
                } else {
                  //console.log (JSON.stringify(body[0]));
                  res.status(401).send('No autorizado')
                }
            }
        });
    }
};

// logout
exports.logout = (req, res, next) => {
    let jwtUser = req.user; // Tengo aqui la información del usuario logado incluida en el jwt
    blacklist.revoke(req.user);  // Revoco el token
    let userId = jwtUser.userId;

    let queryString = 'q={"userId":' + userId + '}&'; // Traigo todos los datos del usuario
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('user?' + queryString + apikeyMLab,
        function(err, respuestaMLab, body) {
            if (err) {
                res.status(500).send('Mlab - Error obteniendo usuario');
            } else {
                if (body.length > 0) {
                    let updatedUser = body[0];
                    updatedUser.isLogged = false;
                    let idUpdate = body[0]._id.$oid;
                    let urlPut = 'user/' + idUpdate + '?' + apikeyMLab;
                    httpClient.put(urlPut, updatedUser, (err, resMLab, bodyP) => {
                        if (err) {
                            res.status(500).send('Mlab - Error al intentar actualizar estado de usuario');
                        } else {
                            updatedUser.password = undefined; // Para que no viaje en la respuesta
                            res.status(204).send('Usuario deslogado correctamente');
                        }
                    });
                } else {
                    res.status(404).send({"error": 'Usuario no encontrado'});
                }
            }
        });
};

exports.changePassword = (req, res, next) => {
    let jwtUser = req.user; // Tengo aqui la información del usuario logado incluida en el jwt
    let userId = jwtUser.userId;
    let newPasw = req.headers.passw; // La nueva password debe venir en header "passw"

    let queryString = 'q={"userId":' + userId + '}&'; // Traigo todos los datos del usuario
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('user?' + queryString + apikeyMLab,
        function(err, respuestaMLab, body) {
            if (err) {
                res.status(500).send({"error":'Mlab - Error obteniendo usuario'});
            } else {
                if (body.length > 0) {
                    let updatedUser = body[0];
                    // Como la passw viene hasheada desde front solo puedo valida que venga una y que no sea igual a la actual.
                    if ( (!newPasw) || (updatedUser.password == newPasw) ) {
                        res.status(422).send({'error': "Nueva password no valida"});
                    } else {
                        updatedUser.password = newPasw; // La nueva que se recibe en header passw
                        let idUpdate = body[0]._id.$oid;
                        let urlPut = 'user/' + idUpdate + '?' + apikeyMLab;
                        httpClient.put(urlPut, updatedUser, (err, resMLab, bodyP) => {
                            if (err) {
                                res.status(500).send({"error": "Mlab - Error al intentar actualizar usuario"});
                            } else {
                                updatedUser.password = undefined; // Para que no viaje en la respuesta
                                res.status(200).send({ "message": "Usuario actualizado", updatedUser });
                            }
                        });
                    }

                } else {
                    res.status(404).send('Usuario no encontrado');
                }
            }
        });
};

