'use strict';
const config = require('../../../config'); 

var requestJSON = require('request-json');
const Logger = require('node-json-logger');
const logger = new Logger({ level: config.logerLevel});

const API_BASE_URL = config.API_BASE_URL;
const baseMLabURL = config.baseMLabURL;
const apiKey = config.apiKey;
const apikeyMLab = `apiKey=${apiKey}`;

// get All User Accounts
exports.getAllUserAccounts = (req, res, next) => {
    let jwtUser = req.user;
    let expand = req.query.expand;

    let queryString = 'q={"userId":' + jwtUser.userId + '}&f={"_id":0, "first_name":0, "last_name": 0, "avatar":0, "password":0, "accounts.transactions":0}&';
    if ( expand == "all") {
        queryString = 'q={"userId":' + jwtUser.userId + '}&f={"_id":0}&';
    }
    logger.info(`Id usuario requerido ${jwtUser.userId}`);
    logger.info(`GET ${ API_BASE_URL }accounts/`);
    console.log("**** getAllUserAccounts ****")
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('user?' + queryString + apikeyMLab,
        function(err, respuestaMLab, body) {
            var response = {};
            if (err) {
                res.status(500).send('Error obteniendo usuario');
            } else {
                if (body.length > 0) {
                    response = body[0];
                } else {
                    res.status(404).send('Usuario no encontrado');
                }
            }
            res.send(response);
        });
};

// Dada una cuenta usuario/customer retorna el mayor id de cuenta que tenga.
function maxAccountId (user) {
    let maxId = 0;
    if (user && user.accounts) {
        user.accounts.forEach(function(account) {
            if (account.accountId > maxId) {
                maxId = account.accountId;
            }
        });
    }     
    return maxId;
};

exports.postUserAccount = (req, res, next) => {
    let newAccount = req.body;
    let userId = Number(req.headers.userid);
    let queryString = 'q={"userId":' + userId + '}&';

    logger.info (`queryString: ${queryString}`);

    let httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('user?' + queryString + apikeyMLab,
      function(err, respuestaMLab, body) {
        if(err) {
            res.status(500).send('Error obteniendo usuario');
        } else {
            if(body.length > 0) {
                let newUser = body[0]; // Me quedo con los datos del usuario que traigo de mlab
                if (!newUser.accounts) { // Si el usuario no viene con cuentas creo el array
                    newUser.accounts = [];
                }
                newAccount.accountId =  maxAccountId(newUser) + 1; // Seteo accountId
                newUser.accounts.push(newAccount); // Agrego la cuenta  al array de cuentas del usuario
                // Actualizo el usuario en mlab
                let idUpdate = body[0]._id.$oid;
                let urlPut = 'user/' + idUpdate + '?' + apikeyMLab;
                httpClient.put(urlPut, newUser, (err, resMLab, bodyP) => {
                    if (err) {
                        res.status(500).send({"error": "Mlab - Error al intentar actualizar usuario"});
                    } else {
                        let ctas = newUser.accounts;
                        res.status(200).send({ "message": "Cuenta agregada con exito", "accountId": newAccount.accountId } );
                    }
                });
            } else {
                let response = "Error - No existe usuario/cliente indicado";
                res.status(422).send({"error": response});
                logger.info(response);
            }
        }
});};

function filterResponse(userData, accountId) {
    let res = {
        userId: userData.userId,
        name: `${userData.first_name} ${userData.last_name}`,
        rol: userData.rol,
    };
    let i = 0;
    let encontre = false;
    while (i < userData.accounts.length && !encontre ) {
        if (userData.accounts[i].accountId == accountId) {
            res.account = userData.accounts[i];
            encontre = true;
        };
        i++;
    }
    return res;
};


// get a user account
exports.getUserAccount = (req, res, next) => {
    let jwtUser = req.user; 
    let accountId = req.params.accountId;

    //let  queryString = 'q={"userId":' + jwtUser.userId + ', "accounts.accountId":' + accountId + '}&f={"_id":0,"password":0, "accounts.transactions":0}&';
    let  queryString = 'q={"userId":' + jwtUser.userId + '}&f={"_id":0,"password":0, "accounts.transactions":0}&';

    logger.info (`queryString: ${queryString}`);
    logger.info(`Id cta requerida ${accountId}`);
    logger.info(`GET ${ API_BASE_URL }accounts/${accountId}`);
    var httpClient = requestJSON.createClient(baseMLabURL);

    httpClient.get('user?' + queryString + apikeyMLab,
        function(err, respuestaMLab, body) {
            var response = {};
            console.log("body:" + body);
            if (err) {
                res.status(500).send('Error obteniendo cuentas del usuario');
            } else {
                if (body.length > 0) {
                    response = filterResponse(body[0], accountId);
                    res.status(200).send(response);
                } else {
                    res.status(404).send('Usuario no encontrado, no se pudo ubicar el usuario para obtener sus cuentas.');
                }
            }
        });
};

// update a user account
exports.putUserAccount = (req, res, next) => {
    res.status(500).send('Funcionalidad aún no implementada');
};

// delete a user account
exports.deleteUserAccount = (req, res, next) => {
    res.status(500).send('Funcionalidad aún no implementada');
};

// get account transactions
exports.getAccountTransactions = (req, res, next) => {
    let jwtUser = req.user; 
    let accountId = req.params.accountId;

    let  queryString = 'q={"userId":' + jwtUser.userId + ', "accounts.accountId":' + accountId + '}&f={"_id":0,"password":0}&';
    //let  queryString = 'q={"$and":[ {"userId":' + jwtUser.userId + ', "accounts.accountId":' + accountId + '}]}&f={"_id":0,"password":0}&';

    logger.info (`queryString: ${queryString}`);
    logger.info(`Id cta requerida ${accountId}`);
    logger.info(`GET ${ API_BASE_URL }accounts/${accountId}/transactions`);
    var httpClient = requestJSON.createClient(baseMLabURL);

    httpClient.get('user?' + queryString + apikeyMLab,
        function(err, respuestaMLab, body) {
            var response = {};
            console.log("body:" + body);
            if (err) {
                res.status(500).send('Error obteniendo cuentas del usuario');
            } else {
                if (body.length > 0) {
                    response = filterResponse(body[0], accountId);
                    res.status(200).send(response);
                } else {
                    res.status(404).send('Usuario no encontrado, no se pudo ubicar el usuario para obtener sus cuentas.');
                }
            }
        });
    
};

