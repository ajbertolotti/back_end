'use strict';

const router = require('express').Router();

const controller = require('../controllers/usersController');

/*
 * /api/users/     GET    - GET ALL
 * /api/users/     POST   - CREATE
 * /api/users/:id  GET    - GET ONE
 * /api/users/:id  PUT    - UPDATE
 * /api/users/:id  DELETE - DELETE
 */

router.route('/')
    .get(controller.getAll)
    .post(controller.post);

router.route('/:id')
    .get(controller.get)
    .put(controller.put)
    .delete(controller.delete);

module.exports = router;
