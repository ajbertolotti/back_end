'use strict';

const router = require('express').Router();

const controller = require('../controllers/transfersController');

/*
 * /api/transfers/                         POST   - CREATE a transfer
 */

router.route('/')
    .post(controller.postTransfer);

module.exports = router;

