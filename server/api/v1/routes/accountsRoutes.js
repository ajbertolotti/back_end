'use strict';

const router = require('express').Router();

const controller = require('../controllers/accountsController');

/*
 * /api/accounts/                         GET    - GET ALL accounts from a user (userId in header)
 * /api/accounts/                         POST   - CREATE (userId in header)
 * /api/accounts/:accountId               GET    - GET ONE
 * /api/accounts/:accountId               PUT    - UPDATE
 * /api/accounts/:accountId               DELETE - DELETE
 * /api/accounts/:accountId/transactions  GET    - GET transactions for the account
 */

router.route('/')
    .get(controller.getAllUserAccounts)
    .post(controller.postUserAccount);

router.route('/:accountId')
    .get(controller.getUserAccount)
    .put(controller.putUserAccount)
    .delete(controller.deleteUserAccount);

router.route('/:accountId/transactions')
    .get(controller.getAccountTransactions);


module.exports = router;
