'use strict';

const router = require('express').Router();

const controller = require('../controllers/authController');

/*
 * /api/         POST   - CREATE - LOGIN
 * /api/auth     DELETE - DELETE - LOGOUT
 * /api/auth     PUT    - UPDATE - CHANGE PASSWORD
 */

router.route('/')
    .post(controller.login)

router.route('/auth')
    .delete(controller.logout)
    .put(controller.changePassword);

module.exports = router;
