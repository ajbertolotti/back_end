'use strict';

const router = require('express').Router();

const authRoutes = require('./routes/authRoutes');
const usersRoutes = require('./routes/usersRoutes');
const accountsRoutes = require('./routes/accountsRoutes');
const transfersRoutes = require('./routes/transfersRoutes');

router.use('/', authRoutes);
router.use('/users', usersRoutes);
router.use('/accounts', accountsRoutes);
router.use('/transfers', transfersRoutes);

module.exports = router;