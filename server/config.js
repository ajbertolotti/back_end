const API_BASE_URL = '/api-uruguay/'; 
const API_VERSION = 'v1/';
const baseMLabURL = 'https://api.mlab.com/api/1/databases/techudb/collections/';
const apiKey = 'vUC-_YWGspcV3Y2UMj8Coy_zYvmz_4mo';
const API_PORT = 3000;
const JWT_SECRET = 'aj_TechU2018';
const logerLevel = 'debug'; // Valid options are: trace, debug, info, warn, error, fatal
const EXCHANGE_RATE = 30;

module.exports = {  
    API_BASE_URL: API_BASE_URL,
    API_VERSION: API_VERSION,
    baseMLabURL: baseMLabURL,
    apiKey: apiKey,
    API_PORT: API_PORT,
    JWT_SECRET: JWT_SECRET,
    logerLevel: logerLevel,
    EXCHANGE_RATE: EXCHANGE_RATE
}

