# Imagen docker base
FROM node

# Definimos directorio de trabgajo
WORKDIR /docker-api

# Copia archivos de nuestro directorio del proyecto al directorio indicado.
ADD . /docker-api

# Instalar dependencias
RUN npm install
 

# Expone en el puerto 3000
EXPOSE 3000

# Ejecutamos el comando detallado.
CMD ["npm", "run", "startdev"]
