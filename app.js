'use strict';

//var express = require('express'); // Importe el modulo

const app = require('./server/server');
const config = require('./server/config');

// Puerto definido en variable de entorno o en su defecto el configurado.
var port =  process.env.PORT || config.API_PORT; 
app.listen(port);

console.log(`Servidor node ejecutando sobre puerto ${port}`);

