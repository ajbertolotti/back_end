
## BBVA URUGUAY - TECHU Octubre 2018 - Back-End - Alvaro Bertolotti ##

##  Comentarios generales sobre el desarrollo

En general se usa notción ES6, arrow functions, etc pero no en forma exclusiva.
Api version, se implementó versionado de API pero dejando solo una disponible por vez.
El código se organiza en modulos por funcionalidades utilizando express Router.
La configurción se toma de archivo config.js

Para lanzar en local: npm run startdev

Para desplegar contenedor en docker hub:
docker build -t ajbertolotti/img-api-v1 . 
docker push ajbertolotti/img-api-v1

Luego para desplegar en openshift directo sobre la consola:
https://console.starter-us-east-1.openshift.com/console

Repositorio de código en bitbucket:
https://bitbucket.org/ajbertolotti/back_end/src/master/

La API esta desplegada en Openshift, puede ser consumida accediendo a la siguiente URL:
http://img-api-v1-techu-back-end.1d35.starter-us-east-1.openshiftapps.com/

El despliegue se realiza por medio de docker, Imágen en Docker Hub:
https://hub.docker.com/r/ajbertolotti/img-api-v1/

Se puede obtener con comando: docker pull ajbertolotti/img-api-v1

La base mongo está ubicada en mlab y puede ser accedia con:
https://api.mlab.com/api/1/databases/techudb/collections?apiKey=vUC-_YWGspcV3Y2UMj8Coy_zYvmz_4mo

Para mostrar funcionalidades básicas se realizó front cuyo código se encuentra en bitubucket y es desplegado en amazon vía ciclo CI/CD orquestado por Jenkins.
Desplegado:
https://bbva-files.s3.amazonaws.com/cells/apps/BBVA_GLOBAL_UY_API_TESTER/pinwd_techu/master/cellsapp/dev/novulcanize/index.html

Jenkins: 
https://globaldevtools.bbva.com/jenkins/job/BBVA_GLOBAL_UY_API_TESTER/job/pinwd_techu/

Repositorio bitbucket:
https://globaldevtools.bbva.com/bitbucket/projects/PINWD/repos/pinwd_techu/browse

Se disponibliza coleccion postman:
https://www.getpostman.com/collections/6197cb5cf32778578c23

(También se puede importarse desde archivo postman_collection.json)

## Metodos implementadas:


    /api-uruguay/v1/                    POST
    "express": "^4.16.3"                 => Como base para el servidor
    "express-jwt": "^5.3.1",             => Para la generación de jwt y habilitación/bloqueo a rutas no autorizadas
    "express-jwt-blacklist": "^1.1.0",   => Para mantener la lista de jwt caducados
    "morgan": "^1.8.1",                  => Para el logueo automatico de las peticiones http
    "node-json-logger": "0.0.8",         => Para el logueo en formato json de modo que pueda ser interpretado con facilidad con herramientas como Logstash, Grafana, etc. Nivel de logeo configurado en config.js
    "request-json": "^0.6.3",            => Para la realización de las peticiones http.


Login:
 * /api-uruguay/v1/            POST     - CREATE - LOGIN
 * /api-uruguay/v1/auth        DELETE   - DELETE - LOGOUT
 * /api-uruguay/v1/auth        PUT      - UPDATE - CHANGE PASSWORD

Users:
 * /api-uruguay/v1/users/     GET    - GET ALL (Pueden usarse query params expand=accounts anda expand=all)
 * /api-uruguay/v1/users/     POST   - CREATE 
 * /api-uruguay/v1/users/:id  GET    - GET ONE
 * /api-uruguay/v1/users/:id  PUT    - UPDATE
 * /api-uruguay/v1/users/:id  DELETE - DELETE (No implementdo aún)

Accounts:
 * /api-uruguay/v1/accounts/                         GET    - GET ALL cuentas de un usuario/cliente (userId in header)
 * /api-uruguay/v1/accounts/                         POST   - CREATE 
 * /api-uruguay/v1/accounts/:accountId               GET    - GET ONE 
 * /api-uruguay/v1/accounts/:accountId               PUT    - UPDATE (No implementdo aún)
 * /api-uruguay/v1/accounts/:accountId               DELETE - DELETE (No implementdo aún)
 * /api-uruguay/v1/accounts/:accountId/transactions  GET    - GET transacciones/movimientos de una cuenta


Transfers:
 * /api-uruguay/v1/transfers/                         POST   - CREATE - Realizar una transferencia.


## Algunos comentarios sobre determinadas operaciones y de funcionamiento en general

* Para el login se asume el password es hasheado en front por lo que se almacenca en BD como viene.
(es como debe ser, se hashea "cuanto antes se pueda")

* Para la autenticación en el request se debe enviar la contraseña del usuario hasheada en header authorization.

* Dicha operación retorna JWT que debe ser enviado en cada petición también en header authorization, si no se envía se denega el acceso.

* En alta de usuario se valida que no exista ya un usuario con mismo mail, se asigna un idUsuario correlativo.

* El alta de usuario solo puede realizarse por usuario con rol "admin", no se permite realizar alta a usuarios no admin.

* Para el cambio de contraseña se debe enviar la nueva contraseña (ya hasheada) en header "passw".

* Transfer. La transferencia implementada es entre 2 usuarios/cuentas (podría ser el mismo).
  En el body de la petición se deben enviar usuario/cliente ordenante y su cuenta y lo mismo para el beneficiario.
  Se indica importe y moneda de la transferencia.
  Si la moneda de origen de la transferencia es distinta a la moneda del saldo que se está afectando se realiza tipo de cambio.
  A modo de ejemplo si la moneda de la transferencia es en pesos y las 2 cuentas afectadas son en dólares se realiza tipo de cambio para ambas afectando el importe correspondiente en dólares en ambas cuentas.
  Es decir la moneda de la transferencia es totalmente independiente a la moneda de las cuentas que se afectan.
  Por simplicidad el tipo de cambio se toma de config.js

  Entre otras cosa se controla:
    . Existencia de cuentas origen y destino.
    . Usuario/cliente al que se realiza el debito sea el usuario logado.
    . Saldo en cuenta origen sea suficiente para realizar la transferencia.
    

## Sobre componentes utilizados

    "cors": "^2.8.4"                     => Para la habilitación de los mismos.
    "express": "^4.16.3"                 => Como base para el servidor
    "express-jwt": "^5.3.1",             => Para la generación de jwt y habilitación/bloqueo a rutas no autorizadas
    "express-jwt-blacklist": "^1.1.0",   => Para mantener la lista de jwt caducados
    "morgan": "^1.8.1",                  => Para el logueo automatico de las peticiones http
    "node-json-logger": "0.0.8",         => Para el logueo en formato json de modo que pueda ser interpretado con facilidad con herramientas como Logstash, Grafana, etc. Nivel de logeo configurado en config.js
    "request-json": "^0.6.3",            => Para la realización de las peticiones http.


## Algunos endpoints

1. Post Login:
http://img-api-v1-techu-back-end.1d35.starter-us-east-1.openshiftapps.com/api-uruguay/v1
En header autorization debe ir el hash de la clave del usuario.
Servicio retoran jwt que, por definición, es autocontenido.

2. Get Accounts:
http://img-api-v1-techu-back-end.1d35.starter-us-east-1.openshiftapps.com/api-uruguay/v1/accounts
El id del usuario es obtenido por la api a partir del jwt.
Retorna todas las cuentas del usuario/cliente.

3. Get Account:
http://img-api-v1-techu-back-end.1d35.starter-us-east-1.openshiftapps.com/api-uruguay/v1/accounts/3
El id del usuario es obtenido por la api a partir del jwt.
Retorna la cuenta nro 3 del usuario indicado.

4. Get Account transactions:
http://img-api-v1-techu-back-end.1d35.starter-us-east-1.openshiftapps.com/api-uruguay/v1/accounts/2/transactions
Retorna las transacciones de la cuenta nro 2 del usuario logado.
